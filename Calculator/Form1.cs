﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        Double resultValue = 0;
        String currentOperation = "";
        bool operationPerformed = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button_click(object sender, EventArgs e)

        {
            Button button = (Button)sender;
            if (!(button.Text == "." && resultBox.Text.Contains(".")))
                resultBox.Text += button.Text;
            if (resultBox.Text.Contains("C"))
                {
                resultBox.Text = "";
                operationPerformed = false;
                resultValue = 0;
                currentOperation = "";
                }
            operationPerformed = false;
        }


        private void buttonOperator(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (!operationPerformed)
                currentOperation = button.Text;
            resultValue = Double.Parse(resultBox.Text);
            resultBox.Clear();
            operationPerformed = true;
        }

        private void buttonEqual(object sender, EventArgs e)
        {
            switch(currentOperation)
            {
                case "+": resultBox.Text = (resultValue + Double.Parse(resultBox.Text)).ToString(); break;
                case "-": resultBox.Text = (resultValue - Double.Parse(resultBox.Text)).ToString(); break;
                case "×": resultBox.Text = (resultValue * Double.Parse(resultBox.Text)).ToString(); break;
                case "÷": resultBox.Text = (resultValue / Double.Parse(resultBox.Text)).ToString(); break;
                case "%": resultBox.Text = (resultValue * 0.01).ToString(); break;
            }
        }
    }
}
